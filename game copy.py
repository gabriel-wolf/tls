import pygame
import random

# Set up some constants
WIDTH, HEIGHT = 800, 600
ROAD_WIDTH, ROAD_HEIGHT = 200, 200
CAR_SIZE = 50
SPEED = 0.5  # Speed less than 1
LIGHT_SIZE = 20
LIGHT_POSITIONS = {
    'north': (WIDTH // 2 - LIGHT_SIZE // 2, HEIGHT // 2 - ROAD_HEIGHT // 2 - LIGHT_SIZE),
    'east': (WIDTH // 2 + ROAD_WIDTH // 2, HEIGHT // 2 - LIGHT_SIZE // 2),
    'south': (WIDTH // 2 - LIGHT_SIZE // 2, HEIGHT // 2 + ROAD_HEIGHT // 2),
    'west': (WIDTH // 2 - ROAD_WIDTH // 2 - LIGHT_SIZE, HEIGHT // 2 - LIGHT_SIZE // 2)
}
LIGHT_COLORS = [(255, 0, 0), (0, 255, 0)]  # Red, Green
CHANGE_LIGHTS_EVENT = pygame.USEREVENT + 1

class Car:
    DIRECTIONS = {
        # 'north': ((WIDTH // 2, HEIGHT), (WIDTH // 2, 0)),
        # 'east': ((0, HEIGHT // 2), (WIDTH, HEIGHT // 2)),
        # 'south': ((WIDTH // 2, 0), (WIDTH // 2, HEIGHT)),
        # 'west': ((WIDTH, HEIGHT // 2), (0, HEIGHT // 2))

         'south': ((WIDTH // 2, HEIGHT), (WIDTH // 2, 0)),
        'west': ((0, HEIGHT // 2), (WIDTH, HEIGHT // 2)),
        'north': ((WIDTH // 2, 0), (WIDTH // 2, HEIGHT)),
        'east': ((WIDTH, HEIGHT // 2), (0, HEIGHT // 2))
    }

    def __init__(self, start_direction, size):
        self.start, _ = self.DIRECTIONS[start_direction]
        self.end_direction = random.choice(list(self.DIRECTIONS.keys()))
        _, self.end = self.DIRECTIONS[self.end_direction]
        self.pos = list(self.start)
        self.rect = pygame.Rect(*self.start, size, size)
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.direction = start_direction

    def move(self, lights):
        if lights[self.direction] == 1:  # Green light
            if self.end_direction in ['north', 'south']:
                self.pos[1] += SPEED if self.end_direction == 'south' else -SPEED
            else:
                self.pos[0] += SPEED if self.end_direction == 'east' else -SPEED
            self.rect.topleft = self.pos

# Initialize Pygame
pygame.init()

# Create the screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))

# Define the cars and road
cars = []
vertical_road = pygame.Rect(WIDTH // 2 - ROAD_WIDTH // 2, 0, ROAD_WIDTH, HEIGHT)
horizontal_road = pygame.Rect(0, HEIGHT // 2 - ROAD_HEIGHT // 2, WIDTH, ROAD_HEIGHT)

# Define the traffic lights
lights = {'north': 1, 'south': 1, 'east': 0, 'west': 0}  # Start with north and south green

# Set the timer for changing lights
pygame.time.set_timer(CHANGE_LIGHTS_EVENT, 5000)  # 5000 milliseconds = 5 seconds

# Main loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_n:
                cars.append(Car('north', CAR_SIZE))
            elif event.key == pygame.K_e:
                cars.append(Car('east', CAR_SIZE))
            elif event.key == pygame.K_s:
                cars.append(Car('south', CAR_SIZE))
            elif event.key == pygame.K_w:
                cars.append(Car('west', CAR_SIZE))
        elif event.type == CHANGE_LIGHTS_EVENT:
            lights = {direction: (light + 1) % len(LIGHT_COLORS) for direction, light in lights.items()}

    # Fill the screen with white
    screen.fill((255, 255, 255))

    # Draw the roads
    pygame.draw.rect(screen, (128, 128, 128), vertical_road)
    pygame.draw.rect(screen, (128, 128, 128), horizontal_road)

    # Draw and move the cars
    for car in cars[:]:
        pygame.draw.rect(screen, car.color, car.rect)
        car.move(lights)
        if car.rect.collidepoint(car.end):
            cars.remove(car)

        # Draw the end direction on the car
        font = pygame.font.Font(None, 36)
        text = font.render(car.end_direction, True, (255, 255, 255))
        screen.blit(text, car.rect)

    # Draw the traffic lights
    for direction, light in lights.items():
        pygame.draw.circle(screen, LIGHT_COLORS[light], LIGHT_POSITIONS[direction], LIGHT_SIZE)

    # Update the display
    pygame.display.flip()

pygame.quit()
